﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CATTCASS4
{
    //enums, by default assign value from 0 and gets increased by +1.
    //but here, assigning new values to the enum.
    public enum Dinosaurs
    {
        Blue = 20,
        Charlie = 30,
        Delta = 40,
        Eco =50,
        Indominus_rex = 80,

    }

    class Program
    {
        static void Main(string[] args)
        {
            //this is simply display enum vales blue, charlie, delta, eco, indominus_rex.
            Console.WriteLine(" Main - value of blue is " + Dinosaurs.Blue);
            Console.WriteLine(" Main - vaulue of Charlie is " + Dinosaurs.Charlie);
            Console.WriteLine(" Main - value of Delta is " + Dinosaurs.Delta);
            Console.WriteLine(" Main - value od Eco is " + Dinosaurs.Eco);
            Console.WriteLine(" Main - value of Indominus_rex is " + Dinosaurs.Indominus_rex);

            //when we cast them with int, the assigned default values will appaer in output.
            //using console window to show output.
            //printing each enums value throught console window.
            Console.WriteLine(" Main - value of blue is " + (int)Dinosaurs.Blue);
            Console.WriteLine(" Main - vaulue of Charlie is " + (int)Dinosaurs.Charlie);
            Console.WriteLine(" Main - value of Delta is " + (int)Dinosaurs.Delta);
            Console.WriteLine(" Main - value od Eco is " + (int)Dinosaurs.Eco);
            Console.WriteLine(" Main - value of Indominus_rex is " + (int)Dinosaurs.Indominus_rex);


            //this is to hold console window from disappearing.
            Console.ReadLine();

        }
    }
}
